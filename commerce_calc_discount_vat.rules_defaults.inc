<?php

/**
 * Implements hook_default_rules_configuration().
 */
 
function commerce_calc_discount_vat_default_rules_configuration() {
  $items = array();
  $items['rules_calculate_vat_on_discounts'] = entity_import('rules_config', '{ "rules_calculate_vat_on_discounts" : {
    "LABEL" : "Calculate VAT on Discounts",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "5",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce VAT" ],
    "REQUIRES" : [ "commerce_calc_discount_vat", "commerce_product_reference" ],
    "ON" : { "commerce_product_calculate_sell_price" : [] },
    "DO" : [
      { "commerce_calc_discount_vat_apply" : { "commerce_line_item" : [ "commerce_line_item" ] } }
    ]
  }
}');
  return $items;
}