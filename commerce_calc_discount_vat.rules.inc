<?php

/**
 * Implements hook_rules_action_info().
 */
function commerce_calc_discount_vat_rules_action_info() {
  $items['commerce_calc_discount_vat_apply'] = array(
    'label' => t('Calculate VAT on Discounted Line Item'),
    'group' => t('Commerce VAT'),
    'parameter' => array(
      'commerce_line_item' => array(
        'label' => t('Line item'),
        'type' => 'commerce_line_item',
        'wrapped' => TRUE,
      ),
    ),
  );
  return $items;
}